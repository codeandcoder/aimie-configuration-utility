/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wordpress.rasanti.aimieconfutility.model;

import com.wordpress.rasanti.aimieconfutility.view.MainWindow;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uge01006
 */
public class ConfFile {
    
    private final File file;
    private String[] supportedLanguages = null;
    private String activeLanguage = null;
    private Map<String,String> directories = null;
    private Map<String,String> files = null;
    private Map<String,String> webs = null;

    public ConfFile(File file) {
        this.file = file;
    }
    
    public String[] getSupportedLanguages() {
        return supportedLanguages;
    }

    public void setSupportedLanguages(String[] supportedLanguages) {
        String[] formattedLangs = new String[supportedLanguages.length];
        for (int i = 0; i < supportedLanguages.length; i++) {
            formattedLangs[i] = supportedLanguages[i].trim().toLowerCase();
        }
        this.supportedLanguages = formattedLangs;
    }

    public String getActiveLanguage() {
        return activeLanguage;
    }

    public void setActiveLanguage(String activeLanguage) {
        boolean modification = this.activeLanguage != null;
        this.activeLanguage = activeLanguage;
        if (modification) {
            saveFile();
        }
    }

    public Map<String, String> getDirectories() {
        return directories;
    }

    public void setDirectories(Map<String, String> directories) {
        this.directories = directories;
    }
    
    public boolean addDirectory(String pron, String path) {
        if ( this.directories.containsKey(pron) || this.files.containsKey(pron) || this.webs.containsKey(pron) )
            return false;
        
        this.directories.put(pron,path);
        saveFile();
        
        return true;
    }
    
    public void removeDirectory(String pron) {
        if ( this.directories.remove(pron) != null ) {
            saveFile();
        }
    }
    
    public Map<String, String> getFiles() {
        return files;
    }

    public void setFiles(Map<String, String> files) {
        this.files = files;
    }
    
    public boolean addFile(String pron, String path) {
        if ( this.directories.containsKey(pron) || this.files.containsKey(pron) || this.webs.containsKey(pron) )
            return false;
        
        this.files.put(pron,path);
        saveFile();
        
        return true;
    }
    
    public void removeFile(String pron) {
        if ( this.files.remove(pron) != null ) {
            saveFile();
        }
    }
    
    public Map<String, String> getWebs() {
        return webs;
    }

    public void setWebs(Map<String, String> webs) {
        this.webs = webs;
    }
    
    public boolean addWeb(String pron, String path) {
        if ( this.directories.containsKey(pron) || this.files.containsKey(pron) || this.webs.containsKey(pron) )
            return false;
        
        this.webs.put(pron,path);
        saveFile();
        
        return true;
    }
    
    public void removeWeb(String pron) {
        if ( this.webs.remove(pron) != null ) {
            saveFile();
        }
    }
    
    public void saveFile() {
        try {
            try (PrintWriter pw = new PrintWriter(new FileWriter(file))) {
                pw.print("supported-langs = ");
                boolean isFirst = true;
                for (String lang : supportedLanguages) {
                    if ( isFirst ) {
                        isFirst = false;
                    } else {
                        pw.print(" | ");
                    }
                    pw.print(lang);
                }
                pw.println();
                pw.println("active-lang = " + activeLanguage);
                pw.println();
                for (Entry<String,String> dir : directories.entrySet()) {
                    pw.println("dir = " + dir.getKey() + "=" + dir.getValue());
                }
                for (Entry<String,String> f : files.entrySet()) {
                    pw.println("file = " + f.getKey() + "=" + f.getValue());
                }
                for (Entry<String,String> w : webs.entrySet()) {
                    pw.println("web = " + w.getKey() + "=" + w.getValue());
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ConfFile readConfig() {
        File confFile = new File("./config/aimie.conf");
        if ( !confFile.exists() ) {
            initConfFile(confFile);
        }
        
        ConfFile file = new ConfFile(confFile);
        Map<String,String> dirs = new HashMap<>();
        Map<String,String> files = new HashMap<>();
        Map<String,String> webs = new HashMap<>();
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(confFile));
            String line = br.readLine();
            while(line != null) {
                if ( line.contains("supported-langs") ) {
                    String[] confParts = line.split("=");
                    file.setSupportedLanguages(confParts[1].split("\\|"));
                } else if ( line.contains("active-lang")) {
                    String[] confParts = line.split("=");
                    file.setActiveLanguage(confParts[1].trim());
                } else if ( line.contains("dir")) {
                    String[] confParts = line.split("=");
                    dirs.put(confParts[1].trim(), confParts[2].trim());
                } else if ( line.contains("file")) {
                    String[] confParts = line.split("=");
                    files.put(confParts[1].trim(), confParts[2].trim());
                } else if ( line.contains("web")) {
                    String[] confParts = line.split("=");
                    webs.put(confParts[1].trim(), confParts[2].trim());
                }
                line = br.readLine();
            }
            file.setDirectories(dirs);
            file.setFiles(files);
            file.setWebs(webs);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return file;
    }
    
    private static void initConfFile(File file) {
        try {
            if ( !file.getParentFile().exists() ) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();
            
            try (PrintWriter pw = new PrintWriter(new FileWriter(file))) {
                pw.println("supported-langs = es | en");
                pw.println("active-lang = en");
            }
            
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
